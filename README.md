# GoDutch - API
#### GoDutch application backend.

### RUN TARGETS

To run the API in localhost:8080, execute the following Make target:

```make run```

A dockerized Postgres database should have run in localhost:5432 and a Cloudbeaver instance in localhost:9091. If you wish to run those containers separately, execute this Make target:

```make start-db```

There is another target to run everything inside containers, including the API:

```make start```

To stop every container currently running:

```make stop```

### API SPEC

While the app is up and running, Swagger API docs are available in the following URL:

http://localhost:8080/swagger-ui

### CLEAN DATABASE

To clean the dockerized Postgres database, run the following Make target:

```make clean-db```

### CI / CD

Pushes to any branch will trigger tests execution.

Pushes to Master branch will deploy the application in Heroku:

https://godutch-api.herokuapp.com/