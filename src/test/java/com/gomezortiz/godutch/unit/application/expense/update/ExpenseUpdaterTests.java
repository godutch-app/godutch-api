package com.gomezortiz.godutch.unit.application.expense.update;

import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.application.expense.update.ExpenseUpdater;
import com.gomezortiz.godutch.application.expense.update.dto.ExpenseUpdateRequest;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

@MicronautTest
class ExpenseUpdaterTests {

    @Inject
    private ExpenseUpdater updater;

    @Inject
    private ExpenseFinder finder;

    @Inject
    private UserFinder userFinder;

    @Inject
    private ExpenseRepository repository;

    @Captor
    private ArgumentCaptor<Expense> expenseCaptor;

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void should_update_only_modified_value() {

        var expense = ExpenseMother.random().build();
        when(finder.findById(String.valueOf(expense.getId()))).thenReturn(expense);
        var expectedDescription = "Another";
        var request = new ExpenseUpdateRequest(String.valueOf(expense.getId()), expectedDescription, expense.getAmount(), String.valueOf(expense.getUserId()));

        updater.update(request);

        verify(repository).update(expenseCaptor.capture());
        var actual = expenseCaptor.getValue();
        assertEquals(expectedDescription, actual.getDescription());
    }

    @Test
    void should_update_paid_by() {

        var expense = ExpenseMother.random().build();
        when(finder.findById(String.valueOf(expense.getId()))).thenReturn(expense);
        var expectedPaidBy = UserMother.random().build();
        when(userFinder.findById(String.valueOf(expectedPaidBy.getId()))).thenReturn(expectedPaidBy);
        var request = new ExpenseUpdateRequest(String.valueOf(expense.getId()), expense.getDescription(), expense.getAmount(), String.valueOf(expectedPaidBy.getId()));

        updater.update(request);

        verify(repository).update(expenseCaptor.capture());
        var actual = expenseCaptor.getValue();
        assertEquals(expectedPaidBy, actual.getPaidBy());
    }

    @MockBean(ExpenseFinder.class)
    ExpenseFinder finder() {
        return mock(ExpenseFinder.class);
    }

    @MockBean(UserFinder.class)
    UserFinder userFinder() {
        return mock(UserFinder.class);
    }

    @MockBean(ExpenseRepository.class)
    ExpenseRepository repository() {
        return mock(ExpenseRepository.class);
    }
}
