package com.gomezortiz.godutch.unit.application.group.delete;

import com.gomezortiz.godutch.application.expense.delete.ExpenseDeleter;
import com.gomezortiz.godutch.application.group.delete.GroupDeleter;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import com.gomezortiz.godutch.mother.group.GroupMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

@MicronautTest
class GroupDeleterTests {

    @Inject
    private GroupDeleter deleter;

    @Inject
    private GroupFinder groupFinder;

    @Inject
    private GroupRepository repository;

    @Inject
    private ExpenseDeleter expenseDeleter;

    @Test
    void should_delete_group() {

        var expected = GroupMother.random().build();
        when(groupFinder.findById(String.valueOf(expected.getId()))).thenReturn(expected);

        deleter.delete(String.valueOf(expected.getId()));

        verify(expenseDeleter).deleteAllByGroup(expected);
        verify(repository).delete(expected);
    }

    @MockBean(GroupFinder.class)
    GroupFinder groupFinder() {
        return mock(GroupFinder.class);
    }

    @MockBean(GroupRepository.class)
    GroupRepository repository() {
        return mock(GroupRepository.class);
    }

    @MockBean(ExpenseDeleter.class)
    ExpenseDeleter expenseDeleter() {
        return mock(ExpenseDeleter.class);
    }
}
