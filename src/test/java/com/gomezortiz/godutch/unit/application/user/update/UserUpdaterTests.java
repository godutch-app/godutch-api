package com.gomezortiz.godutch.unit.application.user.update;

import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.application.user.update.UserUpdater;
import com.gomezortiz.godutch.application.user.update.dto.UserUpdateRequest;
import com.gomezortiz.godutch.domain.common.exception.AlreadyExists;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.domain.user.UserRepository;
import com.gomezortiz.godutch.mother.user.UserMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

@MicronautTest
class UserUpdaterTests {

    @Inject
    private UserUpdater updater;

    @Inject
    private UserFinder userFinder;

    @Inject
    private UserRepository repository;

    @Captor
    private ArgumentCaptor<User> userCaptor;

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void should_update_only_modified_value() {

        var user = UserMother.random().build();
        when(userFinder.findById(String.valueOf(user.getId()))).thenReturn(user);
        var expectedFirstName = "Another";
        var request = new UserUpdateRequest(String.valueOf(user.getId()), expectedFirstName, user.getLastName1(), user.getLastName2(), user.getEmail());

        updater.update(request);

        verify(repository).update(userCaptor.capture());
        var actual = userCaptor.getValue();
        assertEquals(expectedFirstName, actual.getFirstName());
    }

    @Test
    void should_throw_if_email_already_exists() {

        var user = UserMother.random().build();
        when(userFinder.findById(String.valueOf(user.getId()))).thenReturn(user);
        var existingEmail = "existing@email.com";
        when(repository.existsByEmail(existingEmail)).thenReturn(true);
        var request = new UserUpdateRequest(String.valueOf(user.getId()), user.getFirstName(), user.getLastName1(), user.getLastName2(), existingEmail);

        var e = assertThrows(AlreadyExists.class, () -> updater.update(request));

        assertEquals(String.format(ExceptionConstants.USER_ALREADY_EXISTS, request.getEmail()), e.getMessage());
        verify(repository, never()).update(any());
    }

    @MockBean(UserFinder.class)
    UserFinder userFinder() {
        return mock(UserFinder.class);
    }

    @MockBean(UserRepository.class)
    UserRepository repository() {
        return mock(UserRepository.class);
    }
}
