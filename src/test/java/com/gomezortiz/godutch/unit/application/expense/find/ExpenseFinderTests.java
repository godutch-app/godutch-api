package com.gomezortiz.godutch.unit.application.expense.find;

import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.domain.common.exception.NotFound;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import com.gomezortiz.godutch.mother.group.GroupMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@MicronautTest
class ExpenseFinderTests {

    @Inject
    private ExpenseFinder finder;

    @Inject
    private GroupFinder groupFinder;

    @Inject
    private ExpenseRepository repository;

    @Test
    void should_find_expense_by_id() {

        var expected = ExpenseMother.random().build();
        when(repository.findById(expected.getId())).thenReturn(Optional.of(expected));

        var actual = finder.findById(String.valueOf(expected.getId()));

        assertEquals(expected, actual);
    }

    @Test
    void should_throw_if_expense_not_found() {

        var expected = ExpenseMother.random().build();
        when(repository.findById(expected.getId())).thenReturn(Optional.empty());

        var e = assertThrows(NotFound.class, () -> finder.findById(String.valueOf(expected.getId())));

        assertEquals(String.format(ExceptionConstants.EXPENSE_NOT_FOUND, expected.getId()), e.getMessage());
    }

    @Test
    void should_find_expenses_by_group() {

        var group = GroupMother.random().build();
        when(groupFinder.findById(String.valueOf(group.getId()))).thenReturn(group);

        finder.findByGroup(String.valueOf(group.getId()));

        verify(repository).findByGroup(group);
    }

    @MockBean(GroupFinder.class)
    GroupFinder groupFinder() {
        return mock(GroupFinder.class);
    }

    @MockBean(ExpenseRepository.class)
    ExpenseRepository repository() {
        return mock(ExpenseRepository.class);
    }
}
