package com.gomezortiz.godutch.unit.application.user.delete;

import com.gomezortiz.godutch.application.group.update.GroupUpdater;
import com.gomezortiz.godutch.application.user.delete.UserDeleter;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.user.UserRepository;
import com.gomezortiz.godutch.mother.user.UserMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

@MicronautTest
class UserDeleterTests {

    @Inject
    private UserDeleter deleter;

    @Inject
    private UserFinder userFinder;

    @Inject
    private UserRepository repository;

    @Inject
    private GroupUpdater groupUpdater;

    @Test
    void should_delete_user() {

        var expected = UserMother.random().build();
        when(userFinder.findById(String.valueOf(expected.getId()))).thenReturn(expected);

        deleter.delete(String.valueOf(expected.getId()));

        verify(groupUpdater).removeMemberFromEveryGroup(expected);
        verify(repository).delete(expected);
    }

    @MockBean(UserFinder.class)
    UserFinder userFinder() {
        return mock(UserFinder.class);
    }

    @MockBean(UserRepository.class)
    UserRepository repository() {
        return mock(UserRepository.class);
    }

    @MockBean(GroupUpdater.class)
    GroupUpdater groupUpdater() {
        return mock(GroupUpdater.class);
    }
}
