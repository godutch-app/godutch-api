package com.gomezortiz.godutch.unit.application.user.create;

import com.gomezortiz.godutch.application.user.create.UserCreator;
import com.gomezortiz.godutch.application.user.create.dto.UserCreateRequest;
import com.gomezortiz.godutch.domain.common.exception.AlreadyExists;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.user.UserRepository;
import com.gomezortiz.godutch.mother.user.UserMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@MicronautTest
class UserCreatorTests {

    @Inject
    private UserCreator creator;

    @Inject
    private UserRepository repository;

    @Test
    void should_create_user() {

        var expected = UserMother.random().build();
        var request = new UserCreateRequest(String.valueOf(expected.getId()), expected.getFirstName(), expected.getLastName1(), expected.getLastName2(), expected.getEmail());

        creator.create(request);

        verify(repository).create(expected);
    }

    @Test
    void should_throw_if_user_already_exists() {

        var expected = UserMother.random().build();
        var request = new UserCreateRequest(String.valueOf(expected.getId()), expected.getFirstName(), expected.getLastName1(), expected.getLastName2(), expected.getEmail());
        when(repository.existsByEmail(request.getEmail())).thenReturn(true);

        var e = assertThrows(AlreadyExists.class, () -> creator.create(request));

        assertEquals(String.format(ExceptionConstants.USER_ALREADY_EXISTS, request.getEmail()), e.getMessage());
        verify(repository, never()).create(expected);
    }

    @MockBean(UserRepository.class)
    UserRepository repository() {
        return mock(UserRepository.class);
    }
}
