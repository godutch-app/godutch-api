package com.gomezortiz.godutch.unit.application.group.update;

import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.application.group.update.GroupUpdater;
import com.gomezortiz.godutch.application.group.update.dto.GroupUpdateMembersRequest;
import com.gomezortiz.godutch.application.group.update.dto.GroupUpdateRequest;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.common.exception.AlreadyExists;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import com.gomezortiz.godutch.mother.group.GroupMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

@MicronautTest
class GroupUpdaterTests {

    @Inject
    private GroupUpdater updater;

    @Inject
    private GroupFinder groupFinder;

    @Inject
    private GroupRepository repository;

    @Inject
    private UserFinder userFinder;

    @Captor
    private ArgumentCaptor<Group> groupCaptor;

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void should_update_only_modified_value() {

        var group = GroupMother.random().build();
        when(groupFinder.findById(String.valueOf(group.getId()))).thenReturn(group);
        var expectedName = "Another";
        var request = new GroupUpdateRequest(String.valueOf(group.getId()), expectedName);

        updater.update(request);

        verify(repository).update(groupCaptor.capture());
        var actual = groupCaptor.getValue();
        assertEquals(expectedName, actual.getName());
    }

    @Test
    void should_throw_if_name_already_exists() {

        var group = GroupMother.random().build();
        when(groupFinder.findById(String.valueOf(group.getId()))).thenReturn(group);
        var existingName = "Existing";
        when(repository.existsByName(existingName)).thenReturn(true);
        var request = new GroupUpdateRequest(String.valueOf(group.getId()), existingName);

        var e = assertThrows(AlreadyExists.class, () -> updater.update(request));

        assertEquals(String.format(ExceptionConstants.GROUP_ALREADY_EXISTS, request.getName()), e.getMessage());
        verify(repository, never()).update(any());
    }

    @Test
    void should_add_member() {

        var group = GroupMother.random().build();
        when(groupFinder.findById(String.valueOf(group.getId()))).thenReturn(group);
        var member = UserMother.random().build();
        when(userFinder.findById(String.valueOf(member.getId()))).thenReturn(member);
        var request = new GroupUpdateMembersRequest(String.valueOf(group.getId()), String.valueOf(member.getId()));

        updater.addMember(request);

        verify(repository).update(group.addMember(member));
    }

    @Test
    void should_remove_member() {

        var member = UserMother.random().build();
        when(userFinder.findById(String.valueOf(member.getId()))).thenReturn(member);
        var group = GroupMother.random().members(Collections.singleton(member)).build();
        when(groupFinder.findById(String.valueOf(group.getId()))).thenReturn(group);

        var request = new GroupUpdateMembersRequest(String.valueOf(group.getId()), String.valueOf(member.getId()));

        updater.removeMember(request);

        verify(repository).update(group.removeMember(member));
    }

    @Test
    void should_remove_member_from_every_group() {

        var member = UserMother.random().build();
        var groups = List.of(
                GroupMother.random().members(Collections.singleton(member)).build(),
                GroupMother.random().members(Collections.singleton(member)).build()
        );
        when(repository.findByMember(member)).thenReturn(groups);
        var expected = groups.stream()
                .map(group -> Group.create(group.getId(), group.getName()))
                .collect(Collectors.toList());

        updater.removeMemberFromEveryGroup(member);

        verify(repository, times(expected.size())).update(groupCaptor.capture());
        assertTrue(groupCaptor.getAllValues().containsAll(expected));
    }

    @MockBean(GroupFinder.class)
    GroupFinder groupFinder() {
        return mock(GroupFinder.class);
    }

    @MockBean(GroupRepository.class)
    GroupRepository repository() {
        return mock(GroupRepository.class);
    }

    @MockBean(UserFinder.class)
    UserFinder userFinder() {
        return mock(UserFinder.class);
    }
}
