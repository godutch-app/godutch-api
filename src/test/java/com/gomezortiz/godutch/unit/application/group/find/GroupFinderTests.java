package com.gomezortiz.godutch.unit.application.group.find;

import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.common.exception.NotFound;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import com.gomezortiz.godutch.mother.group.GroupMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@MicronautTest
class GroupFinderTests {

    @Inject
    private GroupFinder finder;

    @Inject
    private GroupRepository repository;

    @Inject
    private UserFinder userFinder;

    @Test
    void should_find_group_by_id() {

        var expected = GroupMother.random().build();
        when(repository.findById(expected.getId())).thenReturn(Optional.of(expected));

        var actual = finder.findById(String.valueOf(expected.getId()));

        assertEquals(expected, actual);
    }

    @Test
    void should_throw_if_group_not_found() {

        var expected = GroupMother.random().build();
        when(repository.findById(expected.getId())).thenReturn(Optional.empty());

        var e = assertThrows(NotFound.class, () -> finder.findById(String.valueOf(expected.getId())));

        assertEquals(String.format(ExceptionConstants.GROUP_NOT_FOUND, expected.getId()), e.getMessage());
    }

    @Test
    void should_find_groups_by_member() {

        var member = UserMother.random().build();
        when(userFinder.findById(String.valueOf(member.getId()))).thenReturn(member);

        finder.findByMember(String.valueOf(member.getId()));

        verify(repository).findByMember(member);
    }

    @MockBean(GroupRepository.class)
    GroupRepository repository() {
        return mock(GroupRepository.class);
    }

    @MockBean(UserFinder.class)
    UserFinder userFinder() {
        return mock(UserFinder.class);
    }
}
