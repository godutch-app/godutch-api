package com.gomezortiz.godutch.unit.application.expense.balance;

import com.gomezortiz.godutch.application.expense.balance.ExpenseBalanceCalculator;
import com.gomezortiz.godutch.application.expense.balance.dto.PaymentResponse;
import com.gomezortiz.godutch.application.expense.balance.dto.UserBalanceResponse;
import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import com.gomezortiz.godutch.mother.group.GroupMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@MicronautTest
public class ExpenseBalanceCalculatorTests {

    @Inject
    private ExpenseBalanceCalculator calculator;

    @Inject
    private GroupFinder groupFinder;

    @Inject
    private ExpenseFinder expenseFinder;

    @Test
    void should_calculate_balance() {

        var members = mockMembers();
        var group = GroupMother.random().members(new HashSet<>(members)).build();
        var groupId = String.valueOf(group.getId());
        when(groupFinder.findById(groupId)).thenReturn(group);
        var expenses = mockExpenses(group, members);
        when(expenseFinder.findByGroup(groupId)).thenReturn(expenses);
        var expectedBalanceAmounts = List.of(-59.15D, -22.55D, 40.85D, 40.85D);
        var expectedPaymentAmounts = List.of(40.85D, 22.55D, 18.30D);

        var actual = calculator.calculateBalance(groupId);

        assertEquals(
                expectedBalanceAmounts,
                actual.getBalance().stream()
                        .map(UserBalanceResponse::getBalance)
                        .map(amount -> new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue())
                        .collect(Collectors.toList())
        );
        assertEquals(
                expectedPaymentAmounts,
                actual.getPayments().stream()
                        .map(PaymentResponse::getAmount)
                        .map(amount -> new BigDecimal(amount).setScale(2, RoundingMode.HALF_UP).doubleValue())
                        .collect(Collectors.toList())
        );
    }

    private List<Expense> mockExpenses(Group group, List<User> members) {
        return List.of(
                ExpenseMother.random().group(group).paidBy(members.get(0)).amount(100D).build(),
                ExpenseMother.random().group(group).paidBy(members.get(1)).amount(10D).build(),
                ExpenseMother.random().group(group).paidBy(members.get(1)).amount(53.40D).build()
        );
    }

    private List<User> mockMembers() {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            users.add(UserMother.random().build());
        }
        return users;
    }

    @MockBean(GroupFinder.class)
    GroupFinder groupFinder() {
        return mock(GroupFinder.class);
    }

    @MockBean(ExpenseFinder.class)
    ExpenseFinder expenseFinder() {
        return mock(ExpenseFinder.class);
    }
}
