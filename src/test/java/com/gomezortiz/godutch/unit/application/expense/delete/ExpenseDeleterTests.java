package com.gomezortiz.godutch.unit.application.expense.delete;

import com.gomezortiz.godutch.application.expense.delete.ExpenseDeleter;
import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import com.gomezortiz.godutch.mother.group.GroupMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

@MicronautTest
class ExpenseDeleterTests {

    @Inject
    private ExpenseDeleter deleter;

    @Inject
    private ExpenseFinder groupFinder;

    @Inject
    private ExpenseRepository repository;

    @Captor
    private ArgumentCaptor<Expense> expenseCaptor;

    @BeforeEach
    void setUp() {
        openMocks(this);
    }

    @Test
    void should_delete_expense() {

        var expected = ExpenseMother.random().build();
        when(groupFinder.findById(String.valueOf(expected.getId()))).thenReturn(expected);

        deleter.delete(String.valueOf(expected.getId()));

        verify(repository).delete(expected);
    }

    @Test
    void should_delete_all_expenses_by_group() {

        var group = GroupMother.random().build();
        var expected = List.of(
                ExpenseMother.random().group(group).build(),
                ExpenseMother.random().group(group).build()
        );
        when(repository.findByGroup(group)).thenReturn(expected);

        deleter.deleteAllByGroup(group);

        verify(repository, times(expected.size())).delete(expenseCaptor.capture());
        assertTrue(expenseCaptor.getAllValues().containsAll(expected));
    }

    @MockBean(ExpenseFinder.class)
    ExpenseFinder expenseFinder() {
        return mock(ExpenseFinder.class);
    }

    @MockBean(ExpenseRepository.class)
    ExpenseRepository repository() {
        return mock(ExpenseRepository.class);
    }
}
