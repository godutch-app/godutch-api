package com.gomezortiz.godutch.unit.application.expense.create;

import com.gomezortiz.godutch.application.expense.create.ExpenseCreator;
import com.gomezortiz.godutch.application.expense.create.dto.ExpenseCreateRequest;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

@MicronautTest
class ExpenseCreatorTests {

    @Inject
    private ExpenseCreator creator;

    @Inject
    private GroupFinder groupFinder;

    @Inject
    private UserFinder userFinder;

    @Inject
    private ExpenseRepository repository;

    @Test
    void should_create_expense() {

        var expected = ExpenseMother.random().build();
        when(groupFinder.findById(String.valueOf(expected.getGroupId()))).thenReturn(expected.getGroup());
        when(userFinder.findById(String.valueOf(expected.getUserId()))).thenReturn(expected.getPaidBy());
        var request = new ExpenseCreateRequest(String.valueOf(expected.getId()), expected.getDescription(), expected.getAmount(), String.valueOf(expected.getGroupId()), String.valueOf(expected.getUserId()));

        creator.create(request);

        verify(repository).create(expected);
    }

    @MockBean(GroupFinder.class)
    GroupFinder groupFinder() {
        return mock(GroupFinder.class);
    }

    @MockBean(UserFinder.class)
    UserFinder userFinder() {
        return mock(UserFinder.class);
    }

    @MockBean(ExpenseRepository.class)
    ExpenseRepository repository() {
        return mock(ExpenseRepository.class);
    }
}
