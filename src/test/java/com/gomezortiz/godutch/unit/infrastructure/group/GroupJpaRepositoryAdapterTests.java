package com.gomezortiz.godutch.unit.infrastructure.group;

import com.gomezortiz.godutch.domain.common.exception.NotValid;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.infrastructure.group.GroupJpaRepository;
import com.gomezortiz.godutch.infrastructure.group.GroupJpaRepositoryAdapter;
import com.gomezortiz.godutch.mother.common.MotherCreator;
import com.gomezortiz.godutch.mother.group.GroupMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import io.micronaut.data.jpa.repository.criteria.Specification;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@MicronautTest
public class GroupJpaRepositoryAdapterTests {

    @Inject
    private GroupJpaRepositoryAdapter repositoryAdapter;

    @Inject
    private GroupJpaRepository groupJpaRepository;

    @Test
    void should_find_by_id() {

        var expected = UUID.randomUUID();

        repositoryAdapter.findById(expected);

        verify(groupJpaRepository).findById(expected);
    }

    @Test
    void should_throw_if_null_id_when_finding_by_id() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.findById(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Id"), e.getMessage());
        verify(groupJpaRepository, never()).findById(any());
    }

    @Test
    void should_find_by_member() {

        var expected = UserMother.random().build();

        repositoryAdapter.findByMember(expected);

        verify(groupJpaRepository).findAll(Mockito.<Specification<Group>>any());
    }

    @Test
    void should_throw_if_null_user_when_finding_by_member() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.findByMember(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Member"), e.getMessage());
        verify(groupJpaRepository, never()).findAll(Mockito.<Specification<Group>>any());
    }

    void should_check_if_group_exists_by_name() {

        var expected = MotherCreator.random().lorem().characters(10, 255);

        repositoryAdapter.existsByName(expected);

        verify(groupJpaRepository).findByName(expected);
    }

    @Test
    void should_throw_if_blank_name_when_checking_if_group_exists_by_name() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.existsByName(StringUtils.EMPTY));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Name"), e.getMessage());
        verify(groupJpaRepository, never()).findByName(any());
    }

    @Test
    void should_create_group() {

        var expected = GroupMother.random().build();

        repositoryAdapter.create(expected);

        verify(groupJpaRepository).save(expected);
    }

    @Test
    void should_throw_if_null_group_when_creating_group() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.create(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Group"), e.getMessage());
        verify(groupJpaRepository, never()).save(any());
    }

    @Test
    void should_update_group() {

        var expected = GroupMother.random().build();

        repositoryAdapter.update(expected);

        verify(groupJpaRepository).update(expected);
    }

    @Test
    void should_throw_if_null_group_when_updating_group() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.update(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Group"), e.getMessage());
        verify(groupJpaRepository, never()).update(any());
    }

    @Test
    void should_delete_group() {

        var expected = GroupMother.random().build();

        repositoryAdapter.delete(expected);

        verify(groupJpaRepository).delete(expected);
    }

    @Test
    void should_throw_if_null_group_when_deleting_group() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.delete(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Group"), e.getMessage());
        verify(groupJpaRepository, never()).delete(any());
    }

    @MockBean(GroupJpaRepository.class)
    GroupJpaRepository groupJpaRepository() {
        return mock(GroupJpaRepository.class);
    }
}
