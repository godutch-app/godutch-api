package com.gomezortiz.godutch.unit.infrastructure.user;

import com.gomezortiz.godutch.domain.common.exception.NotValid;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.infrastructure.user.UserJpaRepository;
import com.gomezortiz.godutch.infrastructure.user.UserJpaRepositoryAdapter;
import com.gomezortiz.godutch.mother.common.MotherCreator;
import com.gomezortiz.godutch.mother.user.UserMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@MicronautTest
public class UserJpaRepositoryAdapterTests {

    @Inject
    private UserJpaRepositoryAdapter repositoryAdapter;

    @Inject
    private UserJpaRepository userJpaRepository;

    @Test
    void should_find_by_id() {

        var expected = UUID.randomUUID();

        repositoryAdapter.findById(expected);

        verify(userJpaRepository).findById(expected);
    }

    @Test
    void should_throw_if_null_id_when_finding_by_id() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.findById(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Id"), e.getMessage());
        verify(userJpaRepository, never()).findById(any());
    }

    @Test
    void should_find_by_email() {

        var expected = MotherCreator.random().internet().emailAddress();

        repositoryAdapter.findByEmail(expected);

        verify(userJpaRepository).findByEmail(expected);
    }

    @Test
    void should_throw_if_blank_email_when_finding_by_email() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.findByEmail(StringUtils.EMPTY));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Email"), e.getMessage());
        verify(userJpaRepository, never()).findByEmail(any());
    }

    @Test
    void should_check_if_user_exists_by_email() {

        var expected = MotherCreator.random().internet().emailAddress();

        repositoryAdapter.existsByEmail(expected);

        verify(userJpaRepository).findByEmail(expected);
    }

    @Test
    void should_throw_if_blank_email_when_checking_if_user_exists_by_email() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.existsByEmail(StringUtils.EMPTY));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Email"), e.getMessage());
        verify(userJpaRepository, never()).findByEmail(any());
    }

    @Test
    void should_create_user() {

        var expected = UserMother.random().build();

        repositoryAdapter.create(expected);

        verify(userJpaRepository).save(expected);
    }

    @Test
    void should_throw_if_null_user_when_creating_user() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.create(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "User"), e.getMessage());
        verify(userJpaRepository, never()).save(any());
    }

    @Test
    void should_update_user() {

        var expected = UserMother.random().build();

        repositoryAdapter.update(expected);

        verify(userJpaRepository).update(expected);
    }

    @Test
    void should_throw_if_null_user_when_updating_user() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.update(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "User"), e.getMessage());
        verify(userJpaRepository, never()).update(any());
    }

    @Test
    void should_delete_user() {

        var expected = UserMother.random().build();

        repositoryAdapter.delete(expected);

        verify(userJpaRepository).delete(expected);
    }

    @Test
    void should_throw_if_null_user_when_deleting_user() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.delete(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "User"), e.getMessage());
        verify(userJpaRepository, never()).delete(any());
    }

    @MockBean(UserJpaRepository.class)
    UserJpaRepository userJpaRepository() {
        return mock(UserJpaRepository.class);
    }
}
