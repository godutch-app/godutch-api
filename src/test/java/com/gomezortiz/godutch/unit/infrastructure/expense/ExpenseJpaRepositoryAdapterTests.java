package com.gomezortiz.godutch.unit.infrastructure.expense;

import com.gomezortiz.godutch.domain.common.exception.NotValid;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.infrastructure.expense.ExpenseJpaRepository;
import com.gomezortiz.godutch.infrastructure.expense.ExpenseJpaRepositoryAdapter;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import com.gomezortiz.godutch.mother.group.GroupMother;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@MicronautTest
public class ExpenseJpaRepositoryAdapterTests {

    @Inject
    private ExpenseJpaRepositoryAdapter repositoryAdapter;

    @Inject
    private ExpenseJpaRepository expenseJpaRepository;

    @Test
    void should_find_by_id() {

        var expected = UUID.randomUUID();

        repositoryAdapter.findById(expected);

        verify(expenseJpaRepository).findById(expected);
    }

    @Test
    void should_throw_if_null_id_when_finding_by_id() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.findById(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Id"), e.getMessage());
        verify(expenseJpaRepository, never()).findById(any());
    }

    @Test
    void should_find_by_group() {

        var expected = GroupMother.random().build();

        repositoryAdapter.findByGroup(expected);

        verify(expenseJpaRepository).findByGroup(expected);
    }

    @Test
    void should_throw_if_null_group_when_finding_by_group() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.findByGroup(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Group"), e.getMessage());
        verify(expenseJpaRepository, never()).findByGroup(any());
    }

    @Test
    void should_create_expense() {

        var expected = ExpenseMother.random().build();

        repositoryAdapter.create(expected);

        verify(expenseJpaRepository).save(expected);
    }

    @Test
    void should_throw_if_null_expense_when_creating_expense() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.create(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Expense"), e.getMessage());
        verify(expenseJpaRepository, never()).save(any());
    }

    @Test
    void should_update_expense() {

        var expected = ExpenseMother.random().build();

        repositoryAdapter.update(expected);

        verify(expenseJpaRepository).update(expected);
    }

    @Test
    void should_throw_if_null_expense_when_updating_expense() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.update(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Expense"), e.getMessage());
        verify(expenseJpaRepository, never()).update(any());
    }

    @Test
    void should_delete_expense() {

        var expected = ExpenseMother.random().build();

        repositoryAdapter.delete(expected);

        verify(expenseJpaRepository).delete(expected);
    }

    @Test
    void should_throw_if_null_expense_when_deleting_expense() {

        var e = assertThrows(NotValid.class, () -> repositoryAdapter.delete(null));

        assertEquals(String.format(ExceptionConstants.REQUIRED_ARGUMENT, "Expense"), e.getMessage());
        verify(expenseJpaRepository, never()).delete(any());
    }

    @MockBean(ExpenseJpaRepository.class)
    ExpenseJpaRepository expenseJpaRepository() {
        return mock(ExpenseJpaRepository.class);
    }
}
