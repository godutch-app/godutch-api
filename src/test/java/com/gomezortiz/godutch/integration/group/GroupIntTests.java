package com.gomezortiz.godutch.integration.group;

import com.gomezortiz.godutch.application.expense.delete.ExpenseDeleter;
import com.gomezortiz.godutch.application.group.create.GroupCreator;
import com.gomezortiz.godutch.application.group.create.dto.GroupCreateRequest;
import com.gomezortiz.godutch.application.group.delete.GroupDeleter;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.application.group.update.GroupUpdater;
import com.gomezortiz.godutch.application.group.update.dto.GroupUpdateMembersRequest;
import com.gomezortiz.godutch.application.group.update.dto.GroupUpdateRequest;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.common.IntegrationTest;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.domain.user.UserRepository;
import com.gomezortiz.godutch.mother.expense.ExpenseMother;
import com.gomezortiz.godutch.mother.group.GroupMother;
import com.gomezortiz.godutch.mother.user.UserMother;
import com.gomezortiz.godutch.rest.group.dto.GroupResponse;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.test.annotation.MockBean;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GroupIntTests extends IntegrationTest {

    @Inject
    private GroupFinder finder;

    @Inject
    private GroupCreator creator;

    @Inject
    private GroupUpdater updater;

    @Inject
    private GroupDeleter deleter;

    @Inject
    private GroupRepository repository;

    @Inject
    private UserFinder userFinder;

    @Inject
    private UserRepository userRepository;

    @Inject
    private ExpenseDeleter expenseDeleter;

    private Group group;
    private User member;
    private User newMember;
    private Expense expense;

    @BeforeEach
    private void setUp() {
        member = UserMother.random().build();
        em.persist(member);
        group = GroupMother.random().members(Collections.singleton(member)).build();
        em.persist(group);
        newMember = UserMother.random().build();
        em.persist(newMember);
        expense = ExpenseMother.random().group(group).paidBy(member).build();
        em.persist(expense);
    }

    @Test
    void should_find_by_id() {

        var expected = GroupResponse.fromGroup(group);

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.GET("/api/groups/" + group.getId()), GroupResponse.class);

        assertEquals(HttpStatus.OK, res.getStatus());
        verify(finder).findById(String.valueOf(group.getId()));
        var actual = res.getBody();
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void should_find_by_member() {

        var expected = Collections.singleton(GroupResponse.fromGroup(group));

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.GET("/api/groups/member/" + member.getId()), Argument.of(Set.class, GroupResponse.class));

        assertEquals(HttpStatus.OK, res.getStatus());
        verify(finder).findByMember(String.valueOf(member.getId()));
        var actual = res.getBody();
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void should_create() {

        var group = GroupMother.random().members(new HashSet<>()).build();
        var request = new GroupCreateRequest(String.valueOf(group.getId()), group.getName());

        var res = httpClient.toBlocking()
                .exchange(HttpRequest.PUT("/api/groups", GroupCreateRequest.class).body(request));

        assertEquals(HttpStatus.CREATED, res.getStatus());
        assertEquals("/api/groups/" + group.getId(), res.getHeaders().get("location"));
        verify(creator).create(request);
        var foundGroup = em.find(Group.class, group.getId());
        assertEquals(group, foundGroup);
    }

    @Test
    void should_update() {

        var expectedName = "Another";
        var request = new GroupUpdateRequest(String.valueOf(group.getId()), expectedName);

        var res = httpClient.toBlocking().exchange(HttpRequest.PATCH("/api/groups", GroupUpdateRequest.class).body(request));

        assertEquals(HttpStatus.NO_CONTENT, res.getStatus());
        verify(updater).update(request);
        var foundGroup = em.find(Group.class, group.getId());
        assertEquals(expectedName, foundGroup.getName());
    }

    @Test
    void should_add_a_member() {

        var request = new GroupUpdateMembersRequest(String.valueOf(group.getId()), String.valueOf(newMember.getId()));
        var res = httpClient.toBlocking().exchange(HttpRequest.PATCH("/api/groups/member/add", GroupUpdateMembersRequest.class).body(request));

        assertEquals(HttpStatus.NO_CONTENT, res.getStatus());
        verify(updater).addMember(request);
        var foundGroup = em.find(Group.class, group.getId());
        assertTrue(foundGroup.getMembers().contains(newMember));
    }

    @Test
    void should_remove_a_member() {

        var request = new GroupUpdateMembersRequest(String.valueOf(group.getId()), String.valueOf(member.getId()));
        var res = httpClient.toBlocking().exchange(HttpRequest.PATCH("/api/groups/member/remove", GroupUpdateMembersRequest.class).body(request));

        assertEquals(HttpStatus.NO_CONTENT, res.getStatus());
        verify(updater).removeMember(request);
        var foundGroup = em.find(Group.class, group.getId());
        assertFalse(foundGroup.getMembers().contains(member));
    }

    @Test
    void should_delete() {

        var res = httpClient.toBlocking().exchange(HttpRequest.DELETE("/api/groups/" + group.getId()));

        assertEquals(HttpStatus.NO_CONTENT, res.getStatus());
        verify(deleter).delete(String.valueOf(group.getId()));
        var foundGroup = em.find(Group.class, group.getId());
        assertNull(foundGroup);
        var foundExpense = em.find(Expense.class, expense.getId());
        assertNull(foundExpense);
    }

    @MockBean(GroupFinder.class)
    GroupFinder finder() {
        return mock(GroupFinder.class, withSettings().useConstructor(repository, userFinder).defaultAnswer(CALLS_REAL_METHODS));
    }

    @MockBean(GroupCreator.class)
    GroupCreator creator() {
        return mock(GroupCreator.class, withSettings().useConstructor(repository).defaultAnswer(CALLS_REAL_METHODS));
    }

    @MockBean(GroupUpdater.class)
    GroupUpdater updater() {
        return mock(GroupUpdater.class, withSettings().useConstructor(finder, repository, userFinder).defaultAnswer(CALLS_REAL_METHODS));
    }

    @MockBean(GroupDeleter.class)
    GroupDeleter deleter() {
        return mock(GroupDeleter.class, withSettings().useConstructor(finder, repository, expenseDeleter).defaultAnswer(CALLS_REAL_METHODS));
    }
}
