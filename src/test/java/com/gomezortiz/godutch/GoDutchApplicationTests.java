package com.gomezortiz.godutch;

import com.gomezortiz.godutch.common.IntegrationTest;
import io.micronaut.runtime.EmbeddedApplication;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class GoDutchApplicationTests extends IntegrationTest {

    @Inject
    EmbeddedApplication<?> application;

    @Test
    void should_be_running() {
        Assertions.assertTrue(application.isRunning());
    }

}
