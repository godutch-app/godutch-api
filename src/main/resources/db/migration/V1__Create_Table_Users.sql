CREATE TABLE users (
	id uuid NOT NULL,
	email varchar(255) NULL,
	first_name varchar(255) NOT NULL,
	last_name_1 varchar(255) NOT NULL,
	last_name_2 varchar(255) NULL,
	PRIMARY KEY (id),
	UNIQUE (email)
);