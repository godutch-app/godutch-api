CREATE TABLE groups_members (
	group_id uuid NOT NULL,
	user_id uuid NOT NULL,
	CONSTRAINT groups_members_pkey PRIMARY KEY (group_id, user_id)
);

ALTER TABLE groups_members ADD CONSTRAINT fk1s7vpypr4cpoitcoryuphu47p FOREIGN KEY (group_id) REFERENCES "groups"(id);
ALTER TABLE groups_members ADD CONSTRAINT fk4cc1uaype9m8649rrnlxrnqoc FOREIGN KEY (user_id) REFERENCES users(id);