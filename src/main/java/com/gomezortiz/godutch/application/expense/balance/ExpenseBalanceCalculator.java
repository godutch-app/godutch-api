package com.gomezortiz.godutch.application.expense.balance;

import com.gomezortiz.godutch.application.expense.balance.dto.ExpenseBalanceResponse;
import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.group.Group;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class ExpenseBalanceCalculator {

    private final GroupFinder groupFinder;
    private final ExpenseFinder expenseFinder;

    @Transactional
    public ExpenseBalanceResponse calculateBalance(String groupId) {
        return new ExpenseBalanceResponse(doCalculate(groupId).getBalance(), doCalculate(groupId).processPayments());
    }

    private ExpenseBalanceResponse doCalculate(String groupId) {

        Group group = groupFinder.findById(groupId);
        ExpenseBalanceResponse balance = ExpenseBalanceResponse.fromUsers(group.getMembers());
        List<Expense> expenses = expenseFinder.findByGroup(groupId);

        return balance.processExpenses(expenses).sorted();
    }
}
