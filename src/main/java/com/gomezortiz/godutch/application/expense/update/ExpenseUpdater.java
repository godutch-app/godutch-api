package com.gomezortiz.godutch.application.expense.update;

import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.application.expense.update.dto.ExpenseUpdateRequest;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.domain.user.User;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.transaction.Transactional;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class ExpenseUpdater {

    private final ExpenseFinder finder;
    private final UserFinder userFinder;
    private final ExpenseRepository repository;

    @Transactional
    public void update(ExpenseUpdateRequest request) {

        Expense expense = finder.findById(request.getId());

        String description = request.getDescription();
        if (StringUtils.isNotBlank(description) && !StringUtils.equals(expense.getDescription(), description)) {
            expense = expense.withDescription(description);
        }

        Double amount = request.getAmount();
        if (amount != null && !amount.equals(expense.getAmount())) {
            expense = expense.withAmount(amount);
        }

        String userId = request.getPaidBy();
        if (StringUtils.isNotBlank(userId) && !StringUtils.equals(String.valueOf(expense.getUserId()), userId)) {
            User paidBy = userFinder.findById(userId);
            expense = expense.withPaidBy(paidBy);
        }

        repository.update(expense);
        log.debug("Expense updated: {}", expense);
    }
}
