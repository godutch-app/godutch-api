package com.gomezortiz.godutch.application.expense.create;

import com.gomezortiz.godutch.application.expense.create.dto.ExpenseCreateRequest;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.user.User;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.transaction.Transactional;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class ExpenseCreator {

    private final GroupFinder groupFinder;
    private final UserFinder userFinder;
    private final ExpenseRepository repository;

    @Transactional
    public void create(ExpenseCreateRequest request) {

        Group group = groupFinder.findById(request.getGroupId());
        User paidBy = userFinder.findById(request.getPaidBy());

        Expense expense = repository.create(request.toExpense(group, paidBy));
        log.debug("New expense created: {}", expense);
    }
}
