package com.gomezortiz.godutch.application.expense.update.dto;

import com.gomezortiz.godutch.application.common.validation.id.Identifier;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Solicitud de actualización de un gasto")
public class ExpenseUpdateRequest {

    @Identifier
    @NotBlank
    @Schema(description = "Identificador único del gasto")
    private String id;

    @Size(max = 255, message = ExceptionConstants.LENGTH_ABOVE_MAX)
    @Schema(description = "Descripción del gasto")
    private String description;

    @Schema(description = "Importe del gasto")
    private Double amount;

    @Identifier
    @Schema(description = "Identificador único del usuario que realiza el pago")
    private String paidBy;
}
