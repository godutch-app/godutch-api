package com.gomezortiz.godutch.application.common.validation.id;

import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = UUIDValidator.class)
@Target({FIELD})
@Retention(RUNTIME)
public @interface Identifier {

    String message() default ExceptionConstants.UUID_NOT_VALID;

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
