package com.gomezortiz.godutch.application.group.find;

import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.common.exception.NotFound;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import com.gomezortiz.godutch.domain.user.User;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class GroupFinder {

    private final GroupRepository repository;
    private final UserFinder userFinder;

    @Transactional
    public Group findById(String id) {
        return repository.findById(UUID.fromString(id))
                .orElseThrow(() -> new NotFound(ExceptionConstants.GROUP_NOT_FOUND, id));
    }

    @Transactional
    public List<Group> findByMember(String userId) {
        User member = userFinder.findById(userId);
        return repository.findByMember(member);
    }
}
