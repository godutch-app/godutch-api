package com.gomezortiz.godutch.application.group.delete;

import com.gomezortiz.godutch.application.expense.delete.ExpenseDeleter;
import com.gomezortiz.godutch.application.group.find.GroupFinder;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.transaction.Transactional;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class GroupDeleter {

    private final GroupFinder finder;
    private final GroupRepository repository;
    private final ExpenseDeleter expenseDeleter;

    @Transactional
    public void delete(String id) {
        Group group = finder.findById(id);
        expenseDeleter.deleteAllByGroup(group);
        repository.delete(group);
        log.debug("Group deleted: {}", group);
    }
}
