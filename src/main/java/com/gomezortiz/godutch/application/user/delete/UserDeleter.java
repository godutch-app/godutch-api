package com.gomezortiz.godutch.application.user.delete;

import com.gomezortiz.godutch.application.group.update.GroupUpdater;
import com.gomezortiz.godutch.application.user.find.UserFinder;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.domain.user.UserRepository;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.transaction.Transactional;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class UserDeleter {

    private final UserFinder finder;
    private final UserRepository repository;
    private final GroupUpdater groupUpdater;

    @Transactional
    public void delete(String id) {
        User user = finder.findById(id);
        groupUpdater.removeMemberFromEveryGroup(user);
        repository.delete(user);
        log.debug("User deleted: {}", user);
    }
}
