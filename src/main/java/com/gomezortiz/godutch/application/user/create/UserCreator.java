package com.gomezortiz.godutch.application.user.create;

import com.gomezortiz.godutch.application.user.create.dto.UserCreateRequest;
import com.gomezortiz.godutch.domain.common.exception.AlreadyExists;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.domain.user.UserRepository;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.transaction.Transactional;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class UserCreator {

    private final UserRepository repository;

    @Transactional
    public void create(UserCreateRequest request) {

        if (repository.existsByEmail(request.getEmail())) {
            throw new AlreadyExists(ExceptionConstants.USER_ALREADY_EXISTS, request.getEmail());
        }

        User created = repository.create(request.toUser());
        log.debug("New user created: {}", created);
    }
}
