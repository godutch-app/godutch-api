package com.gomezortiz.godutch.infrastructure.expense;

import com.gomezortiz.godutch.domain.expense.Expense;
import com.gomezortiz.godutch.domain.expense.ExpenseRepository;
import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.infrastructure.common.JpaRepositoryAdapter;
import io.micronaut.data.annotation.Repository;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class ExpenseJpaRepositoryAdapter extends JpaRepositoryAdapter implements ExpenseRepository {

    private final ExpenseJpaRepository expenseJpaRepository;

    @Override
    @Transactional
    public Optional<Expense> findById(UUID id) {
        validateRequired(id, "Id");
        return expenseJpaRepository.findById(id);
    }

    @Override
    @Transactional
    public List<Expense> findByGroup(Group group) {
        validateRequired(group, "Group");
        return expenseJpaRepository.findByGroup(group);
    }

    @Override
    @Transactional
    public Expense create(Expense expense) {
        validateRequired(expense, "Expense");
        return expenseJpaRepository.save(expense);
    }

    @Override
    @Transactional
    public Expense update(Expense expense) {
        validateRequired(expense, "Expense");
        return expenseJpaRepository.update(expense);
    }

    @Override
    @Transactional
    public void delete(Expense expense) {
        validateRequired(expense, "Expense");
        expenseJpaRepository.delete(expense);
    }
}
