package com.gomezortiz.godutch.infrastructure.common;

import com.gomezortiz.godutch.domain.common.exception.NotValid;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import org.apache.commons.lang3.StringUtils;

public abstract class JpaRepositoryAdapter {

    protected void validateRequired(String value, String param) {
        if (StringUtils.isBlank(value)) {
            throw new NotValid(String.format(ExceptionConstants.REQUIRED_ARGUMENT, param));
        }
    }

    protected void validateRequired(Object value, String param) {
        if (value == null) {
            throw new NotValid(String.format(ExceptionConstants.REQUIRED_ARGUMENT, param));
        }
    }
}
