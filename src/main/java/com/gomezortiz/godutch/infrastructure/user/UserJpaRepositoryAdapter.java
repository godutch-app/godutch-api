package com.gomezortiz.godutch.infrastructure.user;

import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.domain.user.UserRepository;
import com.gomezortiz.godutch.infrastructure.common.JpaRepositoryAdapter;
import io.micronaut.data.annotation.Repository;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class UserJpaRepositoryAdapter extends JpaRepositoryAdapter implements UserRepository {

    private final UserJpaRepository userJpaRepository;

    @Override
    @Transactional
    public Optional<User> findById(UUID id) {
        validateRequired(id, "Id");
        return userJpaRepository.findById(id);
    }

    @Override
    @Transactional
    public Optional<User> findByEmail(String email) {
        validateRequired(email, "Email");
        return userJpaRepository.findByEmail(email);
    }

    @Override
    @Transactional
    public boolean existsByEmail(String email) {
        validateRequired(email, "Email");
        return userJpaRepository.findByEmail(email).isPresent();
    }

    @Override
    @Transactional
    public User create(User user) {
        validateRequired(user, "User");
        return userJpaRepository.save(user);
    }

    @Override
    @Transactional
    public User update(User user) {
        validateRequired(user, "User");
        return userJpaRepository.update(user);
    }

    @Override
    @Transactional
    public void delete(User user) {
        validateRequired(user, "User");
        userJpaRepository.delete(user);
    }
}
