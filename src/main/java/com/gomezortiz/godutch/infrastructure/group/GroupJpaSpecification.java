package com.gomezortiz.godutch.infrastructure.group;

import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.user.User;
import io.micronaut.data.jpa.repository.criteria.Specification;
import lombok.experimental.UtilityClass;

@UtilityClass
public final class GroupJpaSpecification {

    public static Specification<Group> byMember(User member) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.isMember(member, root.get("members")));
    }
}
