package com.gomezortiz.godutch.infrastructure.group;

import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.group.GroupRepository;
import com.gomezortiz.godutch.domain.user.User;
import com.gomezortiz.godutch.infrastructure.common.JpaRepositoryAdapter;
import io.micronaut.data.annotation.Repository;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class GroupJpaRepositoryAdapter extends JpaRepositoryAdapter implements GroupRepository {

    private final GroupJpaRepository groupJpaRepository;

    @Override
    @Transactional
    public Optional<Group> findById(UUID id) {
        validateRequired(id, "Id");
        return groupJpaRepository.findById(id);
    }

    @Override
    @Transactional
    public List<Group> findByMember(User member) {
        validateRequired(member, "Member");
        return groupJpaRepository.findAll(GroupJpaSpecification.byMember(member));
    }

    @Override
    @Transactional
    public boolean existsByName(String name) {
        validateRequired(name, "Name");
        return groupJpaRepository.findByName(name).isPresent();
    }

    @Override
    @Transactional
    public Group create(Group group) {
        validateRequired(group, "Group");
        return groupJpaRepository.save(group);
    }

    @Override
    @Transactional
    public Group update(Group group) {
        validateRequired(group, "Group");
        return groupJpaRepository.update(group);
    }

    @Override
    @Transactional
    public void delete(Group group) {
        validateRequired(group, "Group");
        groupJpaRepository.delete(group);
    }
}
