package com.gomezortiz.godutch.rest.common.exception.handler;

import com.gomezortiz.godutch.domain.common.exception.NotFound;
import com.gomezortiz.godutch.domain.common.exception.util.ExceptionConstants;
import com.gomezortiz.godutch.rest.common.exception.dto.ApiError;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import jakarta.inject.Singleton;

@Produces
@Singleton
@Requires(classes = {NotFound.class, ExceptionHandler.class})
public class NotFoundExceptionHandler implements ExceptionHandler<NotFound, HttpResponse<ApiError>> {

    @Override
    public HttpResponse<ApiError> handle(HttpRequest request, NotFound ex) {
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ExceptionConstants.NOT_FOUND, ex.getMessage());
        return HttpResponse.status(apiError.getStatus()).body(apiError);
    }
}
