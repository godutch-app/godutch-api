package com.gomezortiz.godutch.rest.expense;

import com.gomezortiz.godutch.application.expense.balance.ExpenseBalanceCalculator;
import com.gomezortiz.godutch.application.expense.balance.dto.ExpenseBalanceResponse;
import com.gomezortiz.godutch.application.expense.create.ExpenseCreator;
import com.gomezortiz.godutch.application.expense.create.dto.ExpenseCreateRequest;
import com.gomezortiz.godutch.application.expense.delete.ExpenseDeleter;
import com.gomezortiz.godutch.application.expense.find.ExpenseFinder;
import com.gomezortiz.godutch.application.expense.update.ExpenseUpdater;
import com.gomezortiz.godutch.application.expense.update.dto.ExpenseUpdateRequest;
import com.gomezortiz.godutch.rest.common.RestController;
import com.gomezortiz.godutch.rest.common.swagger.*;
import com.gomezortiz.godutch.rest.expense.dto.ExpenseResponse;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.validation.Validated;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;
import java.util.Set;
import java.util.stream.Collectors;

@Validated
@Controller("/api/expenses")
@RequiredArgsConstructor
@Tag(name = "Expenses", description = "Endpoints to perform operations with expenses")
public class ExpenseController extends RestController {

    private final ExpenseFinder finder;
    private final ExpenseCreator creator;
    private final ExpenseUpdater updater;
    private final ExpenseDeleter deleter;
    private final ExpenseBalanceCalculator balanceCalculator;

    @Get(value = ID_PATH, produces = MediaType.APPLICATION_JSON)
    @OkRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Find an expense by its unique id", description = "Find an expense by its unique id")
    public HttpResponse<ExpenseResponse> findById(@PathVariable String id) {
        return HttpResponse.ok(ExpenseResponse.fromExpense(finder.findById(id)));
    }

    @Get(value = "/group" + ID_PATH, produces = MediaType.APPLICATION_JSON)
    @OkRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Find expenses for a group with a given unique ID", description = "Find expenses for a group with a given unique ID")
    public HttpResponse<Set<ExpenseResponse>> findByGroup(@PathVariable String id) {
        return HttpResponse.ok(
                finder.findByGroup(id)
                        .stream()
                        .map(ExpenseResponse::fromExpense)
                        .collect(Collectors.toSet())
        );
    }

    @Get(value = "/group/balance" + ID_PATH, produces = MediaType.APPLICATION_JSON)
    @OkRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Calculate balance for a group with a given unique ID", description = "Calculate balance for a group with a given unique ID")
    public HttpResponse<ExpenseBalanceResponse> calculateBalanceForGroup(@PathVariable String id) {
        return HttpResponse.ok(balanceCalculator.calculateBalance(id));
    }

    @Put(produces = MediaType.APPLICATION_JSON)
    @CreatedRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Create an expense", description = "Create an expense")
    public HttpResponse<Void> create(HttpRequest<?> httpRequest, @Valid @Body ExpenseCreateRequest request) {
        creator.create(request);
        return createdResponse(httpRequest, request.getId());
    }

    @Patch(produces = MediaType.APPLICATION_JSON)
    @NoContentRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Update a given expense", description = "Update a given expense")
    public HttpResponse<Void> update(@Valid @Body ExpenseUpdateRequest request) {
        updater.update(request);
        return HttpResponse.noContent();
    }

    @Delete(value = ID_PATH, produces = MediaType.APPLICATION_JSON)
    @NoContentRes
    @ErrorRes
    @NotFoundRes
    @Operation(summary = "Delete a given expense", description = "Delete a given expense")
    public HttpResponse<Void> delete(@PathVariable String id) {
        deleter.delete(id);
        return HttpResponse.noContent();
    }
}
