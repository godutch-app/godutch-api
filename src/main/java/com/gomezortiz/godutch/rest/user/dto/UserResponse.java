package com.gomezortiz.godutch.rest.user.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gomezortiz.godutch.domain.user.User;
import io.micronaut.core.annotation.Introspected;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(description = "Usuario que cumple con el criterio de búsqueda")
public class UserResponse {

    @Schema(description = "Identificador único del usuario")
    private String id;

    @Schema(description = "Nombre de pila del usuario")
    private String firstName;

    @Schema(description = "Primer apellido del usuario")
    private String lastName1;

    @Schema(description = "Segundo apellido del usuario")
    private String lastName2;

    @Schema(description = "Correo electrónico del usuario")
    private String email;

    public static UserResponse fromUser(User user) {
        return user != null
                ? new UserResponse(
                String.valueOf(user.getId()),
                user.getFirstName(),
                user.getLastName1(),
                user.getLastName2(),
                user.getEmail())
                : null;
    }
}
