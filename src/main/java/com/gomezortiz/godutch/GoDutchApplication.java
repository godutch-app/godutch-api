package com.gomezortiz.godutch;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
    info = @Info(
            title = "GoDutch API",
            description = "Keep track of your groups' expenses",
            version = "1.0"
    )
)
public class GoDutchApplication {

    public static void main(String[] args) {
        Micronaut.run(GoDutchApplication.class, args);
    }
}
