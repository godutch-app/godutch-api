package com.gomezortiz.godutch.domain.group;

import com.gomezortiz.godutch.domain.user.User;
import io.micronaut.core.util.CollectionUtils;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Value
@Builder
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Entity
@Table(name = "groups")
public class Group implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    UUID id;

    @With
    @Column(name = "name", nullable = false, unique = true)
    String name;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "groups_members",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    Set<User> members;

    public Group addMember(User member) {
        Set<User> newMembers = CollectionUtils.isNotEmpty(members) ? new HashSet<>(members) : new HashSet<>();
        newMembers.add(member);
        return new Group(id, name, newMembers);
    }

    public Group removeMember(User member) {
        Set<User> newMembers = CollectionUtils.isNotEmpty(members) ? new HashSet<>(members) : new HashSet<>();
        newMembers.remove(member);
        return new Group(id, name, newMembers);
    }

    public static Group create(UUID id, String name) {
        return new Group(id, name, new HashSet<>());
    }
}
