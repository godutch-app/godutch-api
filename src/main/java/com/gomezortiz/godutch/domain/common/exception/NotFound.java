package com.gomezortiz.godutch.domain.common.exception;

public class NotFound extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotFound(String message, String id) {
        super(String.format(message, id));
    }
}