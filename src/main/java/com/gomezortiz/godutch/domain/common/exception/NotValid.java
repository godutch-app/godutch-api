package com.gomezortiz.godutch.domain.common.exception;

import java.util.Collections;
import java.util.List;

public class NotValid extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final List<String> errors;

    public NotValid(String message, Object... args) {
        super(String.format(message, args));
        this.errors = Collections.singletonList(String.format(message, args));
    }

    public NotValid(List<String> errors, String message, Object... args) {
        super(String.format(message, args));
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }
}
