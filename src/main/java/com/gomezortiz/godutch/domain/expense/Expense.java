package com.gomezortiz.godutch.domain.expense;

import com.gomezortiz.godutch.domain.group.Group;
import com.gomezortiz.godutch.domain.user.User;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Value
@Builder
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Entity
@Table(name = "expenses")
public class Expense implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    UUID id;

    @With
    @Column(name = "description", nullable = false)
    String description;

    @With
    @Column(name = "amount", nullable = false)
    Double amount;

    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false)
    Group group;

    @With
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    User paidBy;

    @EqualsAndHashCode.Exclude
    @Column(name = "paid_at", nullable = false)
    LocalDateTime paidAt;

    public UUID getGroupId() {
        return group != null ? group.getId() : null;
    }

    public UUID getUserId() {
        return paidBy != null ? paidBy.getId() : null;
    }

    public static Expense create(UUID id, String description, Double amount, Group group, User paidBy) {
        return new Expense(id, description, amount, group, paidBy, LocalDateTime.now());
    }
}
